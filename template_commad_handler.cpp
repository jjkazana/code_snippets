#include <cstdint>
#include <iostream>
#include <tuple>

template<uint32_t OPCODE, typename Clbk>
struct Handler
{
    static constexpr uint32_t opcode = OPCODE;
    Clbk callback;
};

template<typename ...Hs>
struct Mailbox
{
    template<uint32_t Count,
        typename = typename std::enable_if<(Count == 0)>::type>
    static bool foo(uint32_t)
    {
        return true;
    }
    
    template<uint32_t Count,
        typename = typename std::enable_if<(Count > 0)>::type>
    static void foo(uint32_t opcode)
    {
        constexpr auto handler = std::get<Count-1>(_handlers);
        if (handler.opcode == opcode) handler.callback();
        foo<Count-1>(opcode);
    }

    static void bar(uint32_t opcode)
    {
        foo<sizeof...(Hs)>(opcode);
    }

    static std::tuple<Hs...> _handlers;
};

struct Action1
{
    static void operator()()
    {
        std::cout << "Action1 called\n";
    }
};

struct Action2
{
    static void operator()()
    {
        std::cout << "Action2 called\n";
    }
};

struct Action3
{
    static void operator()()
    {
        std::cout << "Action3 called\n";
    }
};

using Mb = Mailbox<
    Handler<1, Action1>,
    Handler<3, Action2>,
    Handler<5, Action3>,
    Handler<7, Action1>
>;

void handle(uint32_t opcode)
{
    Mb::bar(opcode);
}


int main()
{
    handle(1);
    handle(2);
    handle(3);
    handle(5);
}