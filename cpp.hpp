#ifndef SNIPPETS_GUARD
#define SNIPPETS_GUARD

/**
	ONLY_AT_FIRST_CALL() macro will cause functions 
	"called" inside to be executed only once in program lifespan.
	Only exception from this rule is heap cleanup, which would 
	cause all static variables to be reinitialized.
	
	Usage:
		ONLY_AT_FIRST_CALL(
			f1(),
			f2(),
			...
			fn()
		);

*/
#define _MACRO_CONCAT(x,y) x##y
#define MACRO_CONCAT(x,y) _MACRO_CONCAT(x,y)
#define ONLY_AT_FIRST_CALL(...) static bool MACRO_CONCAT(once,__LINE__) = [](){return (__VA_ARGS__, true);}();


#endif
