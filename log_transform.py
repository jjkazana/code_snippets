'''
    Implementation of complex natural logarithm image transformation
    in form:
        z = x+iy <- original coords
        w = u+iv <- new coords

        w = ln(z)
'''

import matplotlib.pyplot as plt
import matplotlib.image  as image
import numpy as np

import copy
from collections import defaultdict

def normalize(mat):
    mat[mat==-np.inf] = 0
    offset = np.min(mat)
    scale =  np.max(mat - offset)
    return (mat - offset)/scale

def absolute(mat):
    mat_re = np.abs(np.real(mat))
    mat_im = np.abs(np.imag(mat))
    return 1j*mat_im + mat_re

def mapped(size, focus=50.0):
    '''
        Returns a lookup matrix, of "new_size" dimensions,
        where each element stores a tuple with pixel location
        in original image

    1. Center image by moving origin from (0,0) to (-Xn/2, -Yn/2)
    2. Apply complex natural log function,
    3. Restore quarter signs,
    4. Normalize output and rescale to original size

    absolute() removes signs, so those needs to be restored.
    To do this, partition image according to reference 
    frame parts, and restore each accordingly
    
                   :
             sub_c : sub_d
             ------+------->
             sub_b : sub_a
                   :
                   V

    And restore them, apply below rule for each part:

                   :
          (-1, -1) : (1, -1)
          ---------+------->
          (-1, 1)  : (1, 1)
                   :
                   V
    '''
    Y,X = size

    Y = int(Y)
    X = int(X)
    x_half = int(X/2)
    y_half = int(Y/2)

    # center image by moving origin from (0,0) to (-Xn/2, -Yn/2)
    mat = np.array([
        complex(x-X/2, y-Y/2)
        for y in range(Y)
        for x in range(X)
    ]).reshape(size)

    mat = np.log(absolute(mat/focus))
    # (y,x)
    # sub_a (1,1) - nothing to be done
    # sub_b (-1, 1)
    mat[y_half:Y, 0:x_half] = -1*np.conjugate(mat[y_half:Y, 0:x_half])
    # sub_c (-1, -1)
    mat[0:y_half, 0:x_half] = -1*mat[0:y_half, 0:x_half]
    # sub_d (1, -1)
    mat[0:y_half, x_half:X] = np.conjugate(mat[0:y_half, x_half:X])

    mat_x = (X - 1)*normalize(np.real(mat))
    mat_y = (Y - 1)*normalize(np.imag(mat))

    return np.array([
        [point for point in zip(ly,lx)]
        for ly,lx in zip(mat_y, mat_x)
    ]).round()

def transform(img, coord_map):
    '''
        Using lookup matrix calculated using "mapped" function,
        create new image 
    '''
    Y,X, _ = coord_map.shape
    new_img = np.zeros(img.shape)
    for y, line in enumerate(coord_map):
        for x, (y_old,x_old) in enumerate(line):
            new_img[y,x] = img[int(y_old), int(x_old)]

    return new_img

def main():
    img = image.imread("comic.png")
    size = (len(img), len(img[0]))
    focus = 70.0

    plt.imshow( transform(img, mapped(size, focus)))
    plt.show()

main()

