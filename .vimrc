set hlsearch
set ts=4 sw=4

nmap <C-j> :cn<CR>
nmap <C-Down> :cn<CR>

nmap <C-k> :cp<CR>
nmap <C-Up> :cp<CR>

nmap <C-h> :tabprevious<CR>
nmap <C-Left> :tabprevious<CR>

nmap <C-l> :tabnext<CR>
nmap <C-Right> :tabnext<CR>

function Clear(pat)
    let pat=substitute(a:pat, "[\\<>]", "", "g")
    return pat
endfunction

nmap <F4> :tabnew <bar> :copen <bar> :exe "grep -iIr -m1 --include=\*.{hpp,cpp} ".Clear(@/)." $w"<CR><CR>